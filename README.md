# conda-constructor

## Build Custom Conda Deployments

This uses the constructor builder to customize and build an installer for any environment with specific packages
wanted/needed.

You will need miniconda to build this installer. Below are the basic commands to build the installer with the included
construct.yaml file and additional conda-forge channel.

- install miniconda
- `conda update -y conda` # This isn't necessary but gives the most up to date packages.
- `conda install -y constructor`
- `constructor x86_64`
